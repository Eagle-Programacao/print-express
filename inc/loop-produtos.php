<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_url( $thumb_id );
?>
<div class="col-lg-3 col-md-3 col-sm-3">
	<figure>
		<img src="<?php echo $thumb_url; ?>">
	</figure>
	<div class="info">
		<h3><?php the_title(); ?></h3>
		<p><?php the_excerpt(); ?></p>
		<p><a href="<?php the_permalink(); ?>" class="link_produtos hvr-wobble-horizontal">Saiba Mais</a></p>
	</div>
</div>