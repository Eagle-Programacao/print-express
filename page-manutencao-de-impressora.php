<?php
	get_header();
	bg_page();
?>

<section class="manutencao">
	<div class="row">
		<div class="container">
			<div class="col-sm-12">
				<h2 class="title_div">MANUTENÇÃO DE IMPRESSORAS</h2>
				<div class="border_div"></div>			
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container">
			<div class="col-sm-12">
				<p>
					A PRINT EXPRESS BRASIL oferece suporte técnico a impressoras, computadores e notebooks colocando à sua
					disposição as melhores ferramentas e profissionais.
					A Print Express é um espaço único para manutenção e insumos para impressão. Conta com mais de 17 anos de
					experiência, com técnicos capacitados e certificados nos mais diversos modelos e marcas de impressoras.

				</p>				
			</div>		
		</div>
	</div>
</section>

<section class="gerenciar_custos assistencia">
	<div class="container">
		<div class="row">
			<h3>ASSISTÊNCIA TÉCNICA</h3>
			<p>Sua impressora está com defeito? Nós podemos resolver pra você!</p>
			<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero entrar em contato com a assistência Técnico
</a>
		</div>
	</div>
</section>	

<?php
	get_footer();
?>