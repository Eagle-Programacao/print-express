<!DOCTYPE html>
<html lang="pt">
<head>
	
	<title><?php bloginfo("name") ?><?php echo wp_title(" | "); ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="theme-color" content="#ffffff">
	
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/manifest.json">
	<link rel="mask-icon" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	  <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/_assets/css/bootstrap.min.css">
	<?php wp_head(); ?>	

	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->		

</head>
<body class="<?php if(is_front_page()){ echo "home";}else{echo 'internas';} ?>">
	<div class="mobile">
			<div class="row menu">
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link Active" href="<?php echo get_bloginfo('url'); ?>">
							<i class="fa fa-home" aria-hidden="true"></i>
							Home
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/sobre/">
							<i class="fa fa-university" aria-hidden="true"></i>					
							Quem Somos
						</a>
					</li>					  
					<li class="nav-item">
						<a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/outsourcing/">
							<i class="fa fa-print" aria-hidden="true"></i>					
							Outsourcing
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/vitrine/">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>						
							Produtos
						</a>
					</li>				  
					<li class="nav-item">
						<a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/manutencao-de-impressora/">
						<i class="fa fa-print" aria-hidden="true"></i>	Manutenção de Impressoras</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/blog/">
							<i class="fa fa-weixin" aria-hidden="true"></i>
							Blog
						</a>
					</li>
					<li class="nav-item">
							<a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/contato/">
						<i class="fa fa-address-book" aria-hidden="true"></i>						
							Contato
						</a>
					</li>
				</ul>		
			</div>	
	</div>

<header>
	<section class="topo">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<i class="fa fa-phone-square" aria-hidden="true"></i> (41) 3333 - 3333
				</div>
				<div class="col-lg-4">
					<form class="search-box" action="<?php bloginfo('url'); ?>" method="GET">
						<input type="text" name="s" class="form-control" placeholder="O que você procura?">
						<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
						<i class="fa fa-window-close search_close" aria-hidden="true"></i>	
					</form>
					<span>
						<i class="fa fa-search open_search" aria-hidden="true"></i>
						<i class="fa fa-skype" aria-hidden="true"></i>
						<i class="fa fa-whatsapp open_whats_fixed" aria-hidden="true"></i>
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</span>
				</div>		
			</div>
		</div>
	</section>
	<section class="menu">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<figure>
						<img src="<?php echo get_bloginfo('template_url') ?>/_assets/img/logo/logo.png">
					</figure>					
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9">
					<div id="nav-icon2" class="hamburguer">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>				
					<ul class="nav justify-content-end">
					  <li class="nav-item">
					    <a class="nav-link Active" href="<?php echo get_bloginfo('url'); ?>">Home</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/sobre/">Quem Somos</a>
					  </li>					  
					  <li class="nav-item">
					    <a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/outsourcing/">Outsourcing</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/vitrine/">Produtos</a>
					  </li>				  
					  <li class="nav-item">
					    <a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/manutencao-de-impressora/">Manutenção de Impressoras</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/blog/">Blog</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="<?php echo get_bloginfo('url'); ?>/contato/">Contato</a>
					  </li>
					</ul>				
				</div>
			</div>
		</div>	
	</section>
</header>

<div class="whats_fixed">
	<i class="fa fa-whatsapp" aria-hidden="true"></i> <div>41 | 9 0000 - 0000</div>
</div>