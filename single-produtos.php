<?php
	get_header();
	bg_page();
?>
<section class="single">
	<div class="container">
		<div class="row">
			<?php
				if (have_posts()):
					while (have_posts()):the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );				
			?>
						<div class="col-lg-5 col-md-5 col-sm-5">
							<figure>
								<img src="<?php echo $thumb_url; ?>">
							</figure>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-7">
							<h3><?php the_title(); ?></h3>
							<p><?php the_content(); ?></p>
							<h4>Compartilhe ou fale conosco:</h4>
							<a href="https://www.facebook.com/printexpressbrasil/?ref=br_rs" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<i class="fa fa-whatsapp open_whats_fixed" aria-hidden="true"></i>								
						</div>
			<?php
					endwhile;
				endif;
			?>
		</div>
		<div class="row face_comments">
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-comments" data-href="https://www.facebook.com/printexpressbrasil/?ref=br_rs" data-width="" data-numposts="3"></div>
		</div>
	</div>
</section>
<section class="recentes">
	<div class="container">
		<h2 class="title_div">Produtos recentes</h2>
		<div class="border_div"></div>	
		<div class="row">
			<?php
				$args = array(
					'post_type' 	=> 'produtos',
					'post_status'	=> 'publish',
					'order'			=> 'ASC',
					'orderby'		=> 'date'
				);				
				$wc_query = new WP_Query( $args );
				if ($wc_query -> have_posts()):
					while ($wc_query -> have_posts()): $wc_query -> the_post();
						include('inc/loop-produtos.php');
					endwhile;
				endif;
			?>			
		</div>
	</div>
</section>
<section class="gerenciar_custos">
	<div class="container">
		<div class="row">
			<h3>Deseja GERENCIAR e CONTROLAR os CUSTOS com impressão na sua empresa?</h3>
			<p>Contrate uma consultoria de Outsourcing de Impressão!</p>
			<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero contratar uma consultoria de outsourcing!</a>
		</div>
	</div>
</section>
<?php
	get_footer();
?>