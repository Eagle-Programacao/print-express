<footer>
	<div class="topo">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8">
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<figure>
						<img src="<?php echo get_bloginfo('template_url') ?>/_assets/img/logo/logo-branca.png">
					</figure>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur est neque, vehicula non lacus tincidunt, mollis lobortis sapien. Maecenas ac quam sit amet lacus tincidunt tristique. Fusce efficitur, ex non vehicula rutrum, elit lacus vestibulum tortor, sit amet placerat quam purus.				
					</p>
					<div class="footer_sociais">
						<a href="https://www.facebook.com/printexpressbrasil/?ref=br_rs"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<i class="fa fa-whatsapp open_whats_fixed" aria-hidden="true"></i>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<h3>
						Posts Mais Recentes
					</h3>
					<ul>
						<?php
							$recent_posts = wp_get_recent_posts();
							foreach( $recent_posts as $recent ){
								echo '<li><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
							}
							wp_reset_query();
						?>					
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<h3>Gostou? Curta nossa página</h3>
					<div class="social_page">
						<div id="fb-root"></div>
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-page" data-href="https://www.facebook.com/printexpressbrasil/?fref=ts" data-tabs="timeline" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/printexpressbrasil/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/printexpressbrasil/?fref=ts">Print Express</a></blockquote></div>
					</div>
					<h3 class="sem_margin_bottom">Mantenha - se Informado!</h3>
					<div class="news">
						<?php echo do_shortcode('[contact-form-7 id="26" title="News"]');?>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<h3>
						Contato
					</h3>
					<h4>
						<i class="fa fa-map-marker" aria-hidden="true"></i>Endereço
					</h4>
					<p>Professor Assis Gonçalves, 111</p>
					<h4>
						<i class="fa fa-phone" aria-hidden="true"></i>Telefone
					</h4>
					<p>41 | 3021 - 5200</p>
					<h4>
						<i class="fa fa-envelope" aria-hidden="true"></i>E-mail
					</h4>
					<p>atendimento@printexpress.com.br</p>							
				</div>
			</div>
		</div>	
	</div>
	<div class="bot">
		<div class="topo">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-sm-7">
					©2017 PRINT EXPRESS. Todos os direitos reservados.
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5">
					<ul class="nav justify-content-end">
						<li><a href="">Blog / </a></li>
						<li><a href="">Sobre Nós / </a></li>
						<li><a href="">Contato</a></li>
					</ul>
				</div>
			</div>
		</div>		
	</div>
</footer>

</body>
</html>