<?php

show_admin_bar(false);

add_theme_support( 'post-thumbnails' );
add_image_size("post-highlight", 500, 290, true );

function theme_scripts() {
	# CSS
	wp_enqueue_style ( 'oswald', 'https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700');
	wp_enqueue_style ( 'bebas-book', get_template_directory_uri() . '/_assets/fonts/BebasNeue-Book.otf');
	wp_enqueue_style ( 'font-awesome', get_template_directory_uri() . '/_assets/css/font-awesome.min.css' );
	wp_enqueue_style ( 'main', get_template_directory_uri() . '/_assets/css/all.min.css' );
	wp_enqueue_style ( 'hover', get_template_directory_uri() . '/_assets/css/hover.css' );
	
	# JS
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'js', get_template_directory_uri() . '/_assets/js/app.min.js');

	wp_localize_script(
		'jquery',
		'print',
		array(
			'template'		=> get_bloginfo('template_url'), 
			'url' 			=> get_bloginfo('url')
		)
	);
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );


$pages = array( "Vitrine","Outsourcing","Manutenção de Impressora","Blog","Contato");
foreach ($pages as $p) {
	$page = get_page_by_title($p, "", "page" );
	if (!$page) {
		wp_insert_post(array(
			'post_content'   => "",
			'post_title'     => $p,
			'post_status'    => 'publish',
			'post_type'      => 'page'
		));
	}
}

function custom_admin() {
	wp_enqueue_script( 'datatable-js', '//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js');
	wp_enqueue_style ( 'datatable-css', '//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css' );
	wp_enqueue_style ( 'custom-admin', get_template_directory_uri() . '/_assets/css/admin.min.css' );
}
add_action('admin_enqueue_scripts', 'custom_admin');
add_action('login_enqueue_scripts', 'custom_admin');

function wp_custom_breadcrumbs() {
 
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '/'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'categoria "' . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
}

function bg_page(){
?>
<?php if(get_field('bg_page', get_the_ID()) != null){ ?>
	<section class="bg_page" style="background-image: url('<?php echo get_field("bg_page", get_the_ID()); ?>');">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p><?php the_title(); ?></p>
					<p><?php echo wp_custom_breadcrumbs(); ?></p>
				</div>				
			</div>
		</div>
	</section>
  <?php }else if(get_post_type() == 'produtos'){ ?>
  <section class="bg_page" style="background-image: url('<?php echo get_field("bg_page", 71); ?>');">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p>Produtos</p>
          <p><?php echo wp_custom_breadcrumbs(); ?></p>
        </div>        
      </div>
    </div>
  </section>
<?php }else{ ?>	
	<section class="title_page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p><?php echo wp_custom_breadcrumbs(); ?></p>
					<p class="title"><?php the_title(); ?></p>					
				</div>				
			</div>
		</div>
	</section>
<?php
	}//fim else
}

    add_action('init', 'type_post_produtos');
 
    function type_post_produtos() { 
        $labels = array(
            'name' => _x('Produtos', 'produtos'),
            'singular_name' => _x('Produtos', 'produtos'),
            'add_new' => _x('Novo Produto', 'Novo produto'),
            'add_new_item' => __('Novo Produto'),
            'edit_item' => __('Editar Produto'),
            'new_item' => __('Novo Produto'),
            'view_item' => __('Ver Produto'),
            'search_items' => __('Procurar Produtos'),
            'not_found' =>  __('Nenhum Produto encontrado'),
            'not_found_in_trash' => __('Nenhum Produto encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Produtos'
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'public_queryable' => true,
            'show_ui' => true,
            'rewrite' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'menu_icon'   => 'dashicons-cart',
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title','editor','thumbnail','comments', 'excerpt')
          );
 
register_post_type( 'produtos' , $args );

  register_taxonomy(
  "vitrine",
        "produtos",
        array(            
          "label" => "Vitrine", 
              "singular_label" => "Vitrine", 
              "rewrite" => true,
              "hierarchical" => true
  )
  );
}

function list_categories_post_type(string $post){

  $customPostTaxonomies = get_object_taxonomies($post);

  if(count($customPostTaxonomies) > 0)
  {
    foreach($customPostTaxonomies as $tax)
    {
      $args = array(
      'orderby' => 'name',
      'show_count' => 0,
      'pad_counts' => 0,
      'hierarchical' => 1,
      'taxonomy' => $tax,
      'title_li' => ''
      );
      wp_list_categories( $args );
    }
  }  
}