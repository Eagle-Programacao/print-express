<?php
	get_header();
	bg_page();
?>

<section class="blog">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-9">
				<?php
					// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args = array(
						'post_type' 	=> 'post',
						'order'     	=> 'DESC',
						// 'posts_per_page'=> '3',
						// 'paged'         => $paged,
						'post_status'	=> 'publish'
					);
					$wc_query = new WP_Query( $args );

		            if ($wc_query -> have_posts()):
		                while ($wc_query -> have_posts()): $wc_query -> the_post();
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_url( $thumb_id );		                	
				?>
					<div class="row">
						<div class="col-lg-5 col-md-5 col-sm-5">
							<figure>
								<img src="<?php echo $thumb_url; ?>">
							</figure>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-7">
							<h3><?php the_title(); ?></h3>
							<p><?php the_excerpt(); ?></p>
							<p><a href="<?php echo get_the_permalink();?>" class="hvr-wobble-horizontal">Leia Mais</a></p>
						</div>					
					</div>
				<?php
					endwhile;				
					endif;
				?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<?php echo get_sidebar(); ?>
			</div>			
		</div>
	</div>
</section>
<?php
	wp_reset_query();
	wp_reset_postdata();
?>
<?php
	get_footer();
?>