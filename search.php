<?php
	get_header();
	bg_page();
?>
<section class="recentes" style="margin-bottom: 4%;">
	<div class="container">
		<h2 class="title_div">RESULTADOS DA BUSCA POR: <?php echo get_search_query(); ?></h2>
		<div class="border_div"></div>	
		<div class="row">
			<?php			
				if (have_posts()):
					while (have_posts()): the_post();
						include('inc/loop-produtos.php');
					endwhile;
				endif;
			?>			
		</div>
	</div>
</section>
<section class="gerenciar_custos">
	<div class="container">
		<div class="row">
			<h3>Deseja GERENCIAR e CONTROLAR os CUSTOS com impressão na sua empresa?</h3>
			<p>Contrate uma consultoria de Outsourcing de Impressão!</p>
			<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero contratar uma consultoria de outsourcing!</a>
		</div>
	</div>
</section>
<?php
	get_footer();
?>