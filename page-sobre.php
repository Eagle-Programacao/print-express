<?php
	get_header();
	bg_page();
?>	
	<section class="sobre_nos">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<figure>
						<img src="<?php echo get_bloginfo('template_url')?>/_assets/img/sobre/quem-somos.jpg">
					</figure>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2>Quem Somos</h2>
					<div class="border_div"></div>

					<p>
						<p class="e_text">E</p>m março de 2001, nasceu a PRINT EXPRESS BRASIL, cuja atividade
						consiste na re-manufatura de cartuchos e toners para impressoras
						 e copiadoras, manutenção de equipamentos de informática e gestão de contratos de
						Impressão.
					</p>
					<p>
						A empresa é constituída por empreendedores de uma única família, que ao longo dos anos têm
						mantido a característica de uma empresa familiar com dedicação, trabalho e acima de tudo
						disciplina.
					</p>
					<p>
						As atividades iniciaram-se modestamente, divididas em espaços físicos distantes onde o setor
						industrial localizava-se no Bairro do Pinheirinho e o escritório comercial foi implantado na
						região Central de Curitiba.
					</p>
					<p>
						Ao longo do crescimento da empresa e já bem estruturada financeiramente mudou-se para um
						espaço maior, podendo assim unir os departamentos industrial e comercial, permitindo o
						melhor desempenho nas operações
					</p>
					<p>	
						Em 2003, com a expansão dos negócios permitiu que as atividades pudessem ser
						desenvolvidas na sede própria, localizada no Bairro Água Verde, escolhida por ser uma região
						que viabiliza bons acessos para entregas na cidade e região.A empresa ampliou seu potencial,
						investindo na mão-de-obra de seus colaboradores e adquirindo equipamentos. Hoje a PRINT
						EXPRESS BRASIL está inserida na busca constante por excelência.
					</p>
					<p>
						Oferecemos desde contratos de fornecimeskynto de suprimentos até Projetos Globais de
						Terceirização de Impressão, envolvendo, equipamentos, servidores de impressão, softwares de
						controle, suprimentos, assistência técnica e gestão.
					</p>
				</div>					
			</div>
		</div>	
	</section>
	<section class="sobre_desc">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
				<h3 style="visibility: hidden; line-height: 1px; position: absolute; top: 0;">Meio amabiente</h3>
					<p>
						PRINT EXPRESS BRASIL junto com você na preservação do NOSSO PLANETA. Em
						parceria com a Secretaria do Meior Ambiente realizamos coleta de cartuchos e
						toners em nossos clientes evitando assim o descarte indevido. Cartuchos,
						toners e tinta são recolhidos e devidamente descartados segundo a legislação
						colaborando assim para um mundo melhor.
					</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<figure>
						<img src="<?php echo get_bloginfo('template_url');?>/_assets/img/sobre/desc.jpg">
					</figure>
				</div>
			</div>
		</div>
	</section>
	<section class="caracteristicas_sobre">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<h2 class="title_div">NOSSA MISSÃO</h2>
					<div class="border_div"></div>
					<P>
						<p class="e_text">E</p>
						Entender as necessidades e
						oportunidades de nossos clientes e
						oferecer soluções de impressão mais rápidas e
						melhores do que qualquer outro competidor.
						Conduzir os negócios de forma rentável para
						sustentar nosso crescimento contínuo. Buscar
						a s a t i s f a ç ã o d o s n o s s o s c l i e n t e s ,
						colaboradores e contribuindo para o bem
						estar da comunidade.

					</P>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<h2 class="title_div">NOSSA VISÃO</h2>
					<div class="border_div"></div>		
					<P>
						<p class="e_text">E</p>
						Ser referência em Outsourcing de
						Impressão e Suprimentos (cartuchos,
						toners e informática)a curto/médio prazo,
						buscando apr imoramento contínuo e
						inovando sempre

					</P>		
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<h2 class="title_div">NOSSOS VALORES</h2>
					<div class="border_div"></div>
					<P>
						<p class="e_text">E</p>
						<ul>
							<li>Profissionalismo</li>
							<li>Respeito</li>
							<li>Confiança</li>
							<li>Credibilidade</li>
							<li>Perseverança</li>
						</ul>
					</P>
				</div>
				<p style="padding: 15px;">A Print Express Brasil está inserida na busca constante por excelência. Sendo assim ampliou sua linha de produtos, passando assim a atender também clientes que
				precisam comprar ou de Assistência Técnica de para Computadores.</p>
			</div>
		</div>
	</section>
	<section class="clientes_sobre">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2 class="title_div">NOSSOS CLIENTES</h2>
					<div class="border_div"></div>

					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/tim.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/uci.png">				
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/madero.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/max-flex.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/mini-preco.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/aqua.png">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2 class="title_div">O QUE OS CLIENTES DIZEM</h2>
					<div class="border_div"></div>

					<div class="texto">
						<p>
							PRINT EXPRESS BRASIL junto com você na preservação do
							NOSSO PLANETA. Em parceria com a Secretaria do Meior
							Ambiente realizamos coleta de cartuchos e toners em
							nossos clientes evitando assim o descarte indevido.
							Cartuchos, toners e tinta são recolhidos e devidamente.						
						</p>
					</div>				
				</div>				
			</div>
		</div>
	</section>
	<section class="gerenciar_custos">
		<div class="container">
			<div class="row">
				<h3>Deseja GERENCIAR e CONTROLAR os CUSTOS com impressão na sua empresa?</h3>
				<p>Contrate uma consultoria de Outsourcing de Impressão!</p>
				<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero contratar uma consultoria de outsourcing!</a>
			</div>
		</div>
	</section>	
<?php get_footer(); ?>
