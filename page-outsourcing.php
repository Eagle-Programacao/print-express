<?php
	get_header();
	bg_page();
?>
	<section class="out_impressao">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2 class="title_div">OUTSOURCING DE IMPRESSÃO</h2>
					<div class="border_div"></div>
					<p>
						<p class="e_text">O</p>serviços de Outsourcing de Impressão tem por objetivo otimizar as
						operações de impressão/cópia através de um taxa fixa referente à
						estrutura disponível e um custo por página, onde os ativos e insumos
						envolvidos ficam sob responsabilidade da Print Express. Desta maneira, o
						cliente fica livre dos custos de ativo, depreciação e insumos, pagando uma taxa
						fixa mensal e uma variável inteiramente gerenciável.
					</p>
					<p>
						A solução de Outsourcing de Impressão possui aspectos de Hardware, Serviços
						e Suporte Técnico da mais alta qualidade, totalmente alinhados as
						necessidades do cliente para garantir aos usuários um elevado padrão em
						impressão/cópia de documentos.

					</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<figure>
						<img src="<?php echo get_bloginfo('template_url');?>/_assets/img/outsourcing/outsourcing.jpg">
					</figure>
					<a href="" class="hvr-wobble-horizontal solicitar_orcamento">Solicite um Orçamento</a>
				</div>
			</div>
		</div>
	</section>
	<section class="beneficios">
		<div class="container">
			<div cass="row">
				<h2 class="title_div">PRINCIPAIS BENEFÍCIOS</h2>
				<div class="border_div"></div>						
			</div>			
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<ul>
						<li><a href="">Gerenciamento e controle de custos.</a></li>
						<li><a href="">Rateio de despesa por usuários com base na utilização.</a></li>
						<li><a href="">Redução dos custos com Help Desk.</a></li>
						<li><a href="">Gerenciamento do processo de impressão e cópia.</a></li>
						<li><a href="">Diminuição dos ativos.</a></li>
						<li><a href="">Eliminação das compras de consumíveis</a></li>
						<li><a href="">Operação pró-ativa.</a></li>
					</ul>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<ul>				
						<li><a href="">Diminuição de TCO.</a></li>
						<li><a href="">Redução de fornecedores.</a></li>
						<li><a href="">Manutenção e suporte padronizados.</a></li>
						<li><a href="">Conversão do custo fixo de impressão em variável.</a></li>
						<li><a href="">Diminuição da quantidade de equipamentos e do consumo de energia.</a></li>
						<li><a href="">Aumento da qualidade e disponibilidade dos recursos de impressão e cópia.</a></li>
						<li><a href="">Atualização Tecnológica do parque de impressoras sem desembolso imediato.</a></li>
					</ul>
				</div>				
			</div>
		</div>
	</section>
	<section class="marcas">
		<div class="container">
			<div class="row">
				<figure>
					<img src="<?php echo get_bloginfo('template_url')?>/_assets/img/outsourcing/hp.png">
				</figure>
				<figure>
					<img src="<?php echo get_bloginfo('template_url')?>/_assets/img/outsourcing/epson.png">
				</figure>
				<figure>
					<img src="<?php echo get_bloginfo('template_url')?>/_assets/img/outsourcing/brother.png">
				</figure>
				<figure>
					<img src="<?php echo get_bloginfo('template_url')?>/_assets/img/outsourcing/lexmark.png">
				</figure>
				<figure>
					<img src="<?php echo get_bloginfo('template_url')?>/_assets/img/outsourcing/cannon.png">
				</figure>				
			</div>
		</div>
	</section>
	<section class="gerenciar_custos">
		<div class="container">
			<div class="row">
				<h3>Deseja GERENCIAR e CONTROLAR os CUSTOS com impressão na sua empresa?</h3>
				<p>Contrate uma consultoria de Outsourcing de Impressão!</p>
				<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero contratar uma consultoria de outsourcing!</a>
			</div>
		</div>
	</section>	
<?php get_footer(); ?>
