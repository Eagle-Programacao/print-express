<?php get_header(); ?>
<section class="custom_slider">
	<?php echo do_shortcode("[rev_slider home]"); ?>
</section>
<section class="destaques_home">
	<div class="bd">
		<div class="container">
			<div class="row">
				<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg">
				    <div class="modal-content">
				    	<div class="topo_modal">
				    		<i class="fa fa-window-close hvr-pulse" aria-hidden="true"></i>
				    	</div>
						<div class="destaque_home_modal um">
							<figure>
								<img src="<?php echo get_bloginfo("template_url");?>/_assets/img/destaques/home/um.jpg">
							</figure>
						</div>
						<div class="destaque_home_modal dois">
							<figure>
								<img src="<?php echo get_bloginfo("template_url");?>/_assets/img/destaques/home/dois.jpg">
							</figure>
						</div>
						<div class="destaque_home_modal tres">
							<figure>
								<img src="<?php echo get_bloginfo("template_url");?>/_assets/img/destaques/home/tres.jpg">
							</figure>
						</div>
				    </div>
				  </div>
				</div>			
				<div class="col-lg-4 col-lmd-4 col-sm-4">
					<figure>
						<figcaption>
							<div class="options">
								<i class="fa fa-search hvr-wobble-top" id="destaque_home_icon_um"  aria-hidden="true"></i>
							</div>
							<a href="">
								<img src="<?php echo get_bloginfo("template_url");?>/_assets/img/destaques/home/um.jpg">
							</a>
						</figcaption>
					</figure>
					<div class='info'>
						<h3>
							Manutenção de Impressoras
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.
						</p>
						<p class="leia-mais"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><a href="">Leia Mais</a></p>
					</div>
				</div>

				<div class="col-lg-4 col-lmd-4 col-sm-4">
					<figure>
						<figcaption>
							<div class="options">
								<i class="fa fa-search hvr-wobble-top" id="destaque_home_icon_dois" aria-hidden="true"></i>
							</div>
							<a href="">
								<img src="<?php echo get_bloginfo("template_url");?>/_assets/img/destaques/home/dois.jpg">
							</a>
						</figcaption>
					</figure>
					<div class='info'>
						<h3>
							Cartuchos Compatíveis
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.
						</p>
						<p class="leia-mais"><i class="fa fa-chevron-circle-right" value="tres" aria-hidden="true"></i><a href="">Leia Mais</a></p>
					</div>			
				</div>

				<div class="col-lg-4 col-lmd-4 col-sm-4">
					<figure>
						<figcaption>
							<div class="options">
								<i class="fa fa-search hvr-wobble-top" id="destaque_home_icon_tres" aria-hidden="true"></i>
							</div>
							<a href="">
								<img src="<?php echo get_bloginfo("template_url");?>/_assets/img/destaques/home/tres.jpg">
							</a>
						</figcaption>
					</figure>
					<div class='info'>
						<h3>
							Outsourcing de Impressoras
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.
						</p>
						<p class="leia-mais"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><a href="">Leia Mais</a></p>
					</div>			
				</div>
			</div>
		</div>	
	</div>
</section>
<section class="destaque_corporativo">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<iframe width="100%" height="400" src="https://youtube.com/embed/u548qyxRTBU" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h3>Mensagem da diretoria</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam libero augue, dignissim sed leo in, congue consequat sem. Curabitur non elementum sem. Morbi at maximus tortor. Ut eu blandit lorem, ac fringilla nunc. Aliquam quis convallis libero. Proin non odio eget libero ultrices egestas sit amet at quam. Mauris imperdiet dolor at nibh vulputate, at congue velit elementum. Sed finibus et metus a aliquet. Sed malesuada ullamcorper odio, quis tempor nibh lacinia vel.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam libero augue, dignissim sed leo in, congue consequat sem. Curabitur non elementum sem. Morbi at maximus tortor. Ut eu blandit lorem, ac fringilla nunc. Aliquam quis convallis libero. Proin non odio eget libero ultrices egestas sit amet at quam. Mauris imperdiet dolor at nibh vulputate, at congue velit elementum. Sed finibus et metus a aliquet. Sed malesuada ullamcorper odio, quis tempor nibh lacinia vel.
				</p>
				<p class="leia-mais"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><a href="">Leia Mais</a></p>
			</div>
		</div>	
	</div>
</section>
<section class="gerenciar_custos">
	<div class="container">
		<div class="row">
			<h3>Deseja GERENCIAR e CONTROLAR os CUSTOS com impressão na sua empresa?</h3>
			<p>Contrate uma consultoria de Outsourcing de Impressão!</p>
			<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero contratar uma consultoria de outsourcing!</a>
		</div>
	</div>
</section>
<section class="projetos_mais_recentes">
	<div class="container">
		<h3>Projetos Recentes</h3>
		<p class="align">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>	
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h3>Porque escolher a Print Express?</h3>
				<p>
					Maecenas aliquet metus id risus suscipit gravida. Curabitur id massa vestibulum, luctus ipsum eu, malesuada enim. Integer eleifend rhoncus neque, vel pulvinar lectus imperdiet sed. Maecenas sit amet blandit ligula. Duis erat neque, accumsan et erat et, volutpat efficitur justo. Morbi tristique placerat lectus vel ornare.
				</p>
				<div class="projetos_slider">
				<?php echo do_shortcode("[rev_slider projetos-recentes]"); ?>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
					<?php
						$args = array(
							'posts_per_page' => '3',
						    'order'    => 'DESC'
						);				
						query_posts( $args );
						if( have_posts()):
							while( have_posts()) : the_post();

							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_url( $thumb_id );													
					?>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4">
									<figure>
										<img src="<?php echo $thumb_url; ?>">
									</figure>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-6">
									<h4><?php the_title(); ?></h4>
									<p>
										<?php
											$TextoEmpresa = get_the_content();
											echo mb_substr($TextoEmpresa, 0, 110, 'UTF-8');
										?>										
									</p>
									<p class="leia-mais"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><a href="">Leia Mais</a></p>									
								</div>
							</div>
					<?php
							endwhile;
						endif;
					?>										
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>