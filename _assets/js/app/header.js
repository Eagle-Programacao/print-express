jQuery( document ).ready(function( $ ){

	/*Search Box*/
	jQuery(".search-box").fadeOut();
	jQuery(".open_search").click(function( event ){
		jQuery(".search-box").fadeIn();
	});	
	jQuery(".search_close").click(function( event ){
		jQuery(".search-box").fadeOut();
	});

	/*Menu Active*/
	jQuery(".menu .nav-link").click(function( event ){
		jQuery(".menu .nav-link").removeClass("active");
		jQuery(this).addClass("active");
	});

	jQuery(".open_whats_fixed").click(function( event ){
		jQuery(".whats_fixed").toggleClass("active");
	});

	jQuery('#nav-icon2').click(function(){
		jQuery(this).toggleClass('open');
		jQuery('.mobile').toggleClass('active');
	});
});