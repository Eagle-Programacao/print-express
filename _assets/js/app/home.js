jQuery( document ).ready(function( $ ){
	jQuery(".destaques_home").css("min-height", jQuery(".bd").height());

	//Modal Destaques Home Page
	jQuery(".destaques_home .modal").click(function(){jQuery(this).fadeOut();});
	jQuery(".topo_modal").click(function(){jQuery(".destaques_home .modal").fadeOut();});
	jQuery(".destaques_home .options i").click(function( $ ){
		jQuery(".destaque_home_modal").removeClass("active");
		switch(jQuery(this).attr('id')){
			case "destaque_home_icon_um" : jQuery(".destaque_home_modal.um").addClass("active");break;
			case "destaque_home_icon_dois" : jQuery(".destaque_home_modal.dois").addClass("active");break;
			case "destaque_home_icon_tres" : jQuery(".destaque_home_modal.tres").addClass("active");break;
		}
		jQuery(".destaques_home .modal").fadeIn();
	});
	//Fim Modal Destaques Home Page
});