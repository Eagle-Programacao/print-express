<?php
	get_header();
	bg_page();
?>

<section class="contato">
	<div class="container">
		<div class="row mapa">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.5209392511024!2d-49.28317258487086!3d-25.454274983777303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce4808e287e57%3A0x541fb0b67b2a61cc!2sR.+Prof.+Assis+Gon%C3%A7alves%2C+111+-+%C3%81gua+Verde%2C+Curitiba+-+PR%2C+80620-250!5e0!3m2!1spt-BR!2sbr!4v1495024825677" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="row info">
			<div class="col-lg-8 col-md-8 col-sm-8">
				<h2 class="title_div">CONTATO</h2>
				<div class="border_div"></div>
				<?php echo do_shortcode('[contact-form-7 id="45" title="Contato"]');?>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<h2 class="title_div">INFORMAÇÕES CONTATO</h2>
				<div class="border_div"></div>
				<p class="print">PRINT EXPRESS</p>
				<p class="title">
					<i class="fa fa-map-marker" aria-hidden="true"></i>Endereço
				</p>
				<p>Rua Prof. Assis gonçalves, 111<br>Água verde - Curitiba/PR</p>
				<p class="title">
					<i class="fa fa-phone" aria-hidden="true"></i>Telefone
				</p>
				<p>+55 41 | 3220 - 5000</p>
				<p class="title">
					<i class="fa fa-envelope" aria-hidden="true"></i>E-mail
				</p>
				<p>contato@printexpress.com.br</p>
			</div>
		</div>
	</div>
</section>

<?php
	get_footer();
?>