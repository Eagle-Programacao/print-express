<?php
	get_header();
	bg_page();
?>
<section class="produtos">
	<div class="container">
		<div class="row menu">
			<div>
				<?php
					list_categories_post_type('produtos');
				?>
			</div>
		</div>
		<div class="row cont">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="row zoomIn animated">
					<?php
						if (have_posts()):
							while (have_posts()):the_post();
								include('inc/loop-produtos.php');
							endwhile;
						endif;
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	get_footer();
?>